**Si vous êtes symptomatique ( de la fièvre ou une sensation de  fièvre, de la toux, des difficultés respiratoires ) ne prenez pas le  risque de contaminer d’autres personnes, ne produisez pas ! Restez chez vous n’interagissez pas avec d’autres.**

![guide](documentation/fr/FOLDED_FACESHIELD_FRENCH.jpg "Guide d'utilisation")

## Credits

*  Powered and produced by  Volumes
*  Design and videos by     Aruna RATNAYAKE
*  Supported by             Fab City Grand Paris
*  Contributors             Michael ARAUJO (Volumes), Corentin BOITEAU (Volumes), Olivia BRUNET (Volumes), Francesco CINGOLANI (Volumes), Vincent GUIMAS (Ars Longua), Soumaya NADER (Ars Longua), Thibaut SMITH (Ars Longua)